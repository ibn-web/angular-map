'use strict'
angular.module('leaflet-map', [])

	.directive('leaflet', ['$compile', function ($compile) {

		return function ($scope, element, attrs) {
			return Map_Binder_Directive.initialize($scope, element, attrs, $compile)
		}
	}])

	.factory('Map_Binder', function () {
		return Map_Binder_Directive
	})

var Map_Binder_Directive = {

	marker_attributes: {},
	marker_template: null,
	cluster_template: null,
	popup_template: null,
	objects: null,
	markers: null,
	$compile: null,
	$scope: null,
	element: null,
	map: null,
	leaflet_options: {},

	add_markers: function (objects) {
		if (objects.length > 0) {
			for (var i = 0; i < objects.length; ++i) {
				this.add_marker(objects[i], i)
			}
		}
	},

	add_marker: function (seed, i) {
		var self = this
		var location = seed.location
		var loc = seed.loc = [location.lat, location.lon]
		var marker = L.marker(loc, {
				icon: this.generate_icon(seed, i)
			}
		)

		seed.marker = marker

		this.markers.addLayer(marker)

		this.$scope.$emit('marker.created', marker, seed, i)

		if (this.popup_template) {
			var scope = this.$scope.$new()
			scope.seed = seed
			var popup = this.$compile(this.popup_template)(scope)
			marker.bindPopup(popup[0], this.get_popup_options(seed))
		}
	},

	get_popup_options: function (seed) {
			return undefined
	},

	get_cluster_template: function (element) {
		if (element.length == 0)
			return null

		var html = element[0].outerHTML
		return function (cluster) {
			return new L.DivIcon({
				html: html.replace('cluster-count', cluster.getChildCount())
			});
		}
	},

	generate_icon: function (seed, index) {
		return L.divIcon({
			className: 'unangular',
			html: this.marker_template.replace('%index%', index)
		})
	},

	get_popup_template: function (element) {
		if (element.length == 0)
			return null

		return element[0].outerHTML
	},

	get_marker_template: function (element) {
		if (element.length == 0)
			return null

		var attributes = element[0].attributes
		this.marker_attributes = {}
		for (var i = 0; i < attributes.length; i++) {
			this.marker_attributes[attributes[i].name] = attributes[i].value
		}

		element.append('<marker-index>%index%</marker-index>')
		return element.html()
	},

	get_marker_attributes: function (element) {
		if (element.length == 0)
			return {}

		var attributes = element[0].attributes
		var result = {}
		for (var i = 0; i < attributes.length; i++) {
			result[attributes[i].name] = attributes[i].value
		}
		return result
	},

	angularize_markers: function () {
		var self = this
		this.element.find('.unangular').each(function () {
			var marker_element = jQuery(this)
			var i = parseInt(marker_element.find('marker-index').text())
			marker_element.find('marker-index').remove()
			var location = self.objects[i]
			marker_element.removeClass('unangular')
			for (var a in self.marker_attributes) {
				marker_element.attr(a, self.marker_attributes[a])
			}
			var scope = self.$scope.$new()
			scope.seed = location
			self.$compile(marker_element)(scope)

		})
	},

	initialize: function ($scope, element, attrs, $compile) {
		var self = this
		this.element = element
		this.$scope = $scope
		this.$compile = $compile

		// Create the Leaflet Map
		var map = this.map = L.map(element[0], this.leaflet_options)

		var marker_element = element.find('leaflet-marker')
		this.marker_template = this.get_marker_template(marker_element)
		marker_element.remove()

		var popup_element = element.find('[leaflet-popup]')
		this.popup_template = this.get_popup_template(popup_element)
		popup_element.remove()

		var cluster_element = element.find('leaflet-cluster')
		this.cluster_template = this.get_cluster_template(cluster_element)
		cluster_element.remove()

		$scope.$watchCollection(attrs.bindMarkers, function (new_list, old_list) {
			self.update_map(map, new_list)
			map.on('layeradd', function (e) {
				self.angularize_markers()
			});
		})
	},

	update_map: function (map, new_list) {
		var self = this
		self.map = map
		if (!this.markers) {
			var options = {
				iconCreateFunction: this.cluster_template,
				showCoverageOnHover: false,
				maxClusterRadius: 30
			}
			this.$scope.$emit('cluster_layer.create', options)
			this.markers = new L.MarkerClusterGroup(options)
			map.addLayer(this.markers)
		}
		else {
			this.markers.clearLayers()
		}

		this.objects = new_list
		this.add_markers(this.objects)
	}
}
